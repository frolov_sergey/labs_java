package ru.frolov;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class MailTest {
    @Test
    @DisplayName("Проверка на отсутсвие исключений")
    public void test1() {
        Assertions.assertDoesNotThrow(() -> Main.main(new String[]{}));
    }

    @Test
    @DisplayName("Проверка корректности парсинга писем")
    public void test2() throws Exception {
        Main.main(new String[]{});
        Assertions.assertFalse(Main.getMails().isEmpty());
        Assertions.assertEquals(Main.getMails().size(), 1797);
    }

    @Test
    @DisplayName("Проверка корректности составления диаграммы автор-количество писем")
    public void test3() throws Exception {
        Main.main(new String[]{});
        Assertions.assertFalse(Main.getMailCount().isEmpty());
    }

    @Test
    @DisplayName("Проверка получения DSPAM Probability")
    public void test4() throws Exception {
        Main.main(new String[]{});
        Assertions.assertNotNull(Main.getAvgProbability());
    }

    @Test
    @DisplayName("Проверка диаграммы getMailsCountChart")
    public void test5() throws Exception {
        Main.main(new String[]{});
        Assertions.assertNotNull(Main.getMailsCountChart());
    }

    @Test
    @DisplayName("Проверка наличия исходного файла")
    public void test6() throws Exception {
        Main.main(new String[]{});
        Assertions.assertNotNull(Main.getSource());
    }
}

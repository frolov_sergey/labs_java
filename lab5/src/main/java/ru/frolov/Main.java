package ru.frolov;

import org.apache.james.mime4j.Charsets;
import org.apache.james.mime4j.mboxiterator.CharBufferWrapper;
import org.apache.james.mime4j.mboxiterator.MboxIterator;
import org.apache.james.mime4j.parser.AbstractContentHandler;
import org.apache.james.mime4j.parser.MimeStreamParser;
import org.apache.james.mime4j.stream.Field;
import org.jfree.chart.JFreeChart;

import java.io.File;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {
    private static File source;
    private static List<Mail> mails;
    private static Map<String, Long> mailCount;
    private static Double avgProbability;
    private static JFreeChart mailsCountChart;

    public static void main(String[] args) throws Exception {
        Charset charset = Charsets.UTF_8;
        URL url = Main.class.getResource("/mbox.txt");
        source = new File(url.getFile());

        mails = new ArrayList<>();
        for (final CharBufferWrapper message : MboxIterator.fromFile(source).charset(charset).build()) {
            Mail mail = new Mail();
            MimeStreamParser parser = new MimeStreamParser();
            parser.setContentHandler(new MailContentHandler(mail));
            parser.parse(message.asInputStream(charset));
            mails.add(mail);
        }

        avgProbability = mails.stream().map(Mail::getxDSPAMProbability).reduce(Double::sum)
                .map(i -> i / mails.size()).orElse(null);
        System.out.println("Average value X-DSPAM-Probability: " + avgProbability);

        mailCount = mails.stream().collect(Collectors.groupingBy(Mail::getFrom, Collectors.counting()));
        Graph graph = new Graph();
        mailsCountChart = graph.create(mailCount);
        graph.saveToFile(mailsCountChart);
    }

    public static File getSource() {
        return source;
    }

    public static List<Mail> getMails() {
        return mails;
    }

    public static Map<String, Long> getMailCount() {
        return mailCount;
    }

    public static Double getAvgProbability() {
        return avgProbability;
    }

    public static JFreeChart getMailsCountChart() {
        return mailsCountChart;
    }

    private static class MailContentHandler extends AbstractContentHandler {
        private Mail mail;

        public MailContentHandler(Mail mail) {
            this.mail = mail;
        }

        @Override
        public void field(Field field) {
            switch (field.getName()) {
                case "From":
                    mail.setFrom(field.getBody());
                    break;
                case "To":
                    mail.setTo(field.getBody());
                    break;
                case "X-DSPAM-Probability":
                    mail.setxDSPAMProbability(Double.valueOf(field.getBody()));
                    break;
                case "X-DSPAM-Confidence":
                    mail.setxDSPAMConfidence(Double.valueOf(field.getBody()));
                    break;
            }
        }
    }

}

package ru.frolov;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class Graph {
    public static void main(String[] args) {}

    public JFreeChart create(Map<String, Long> source){
        return ChartFactory.createPieChart(
                "Number of emails",   // chart title
                createDataset(source),
                false,
                false,
                false
        );
    }

    public void saveToFile(JFreeChart chart) throws IOException {
        ChartUtils.saveChartAsPNG(new File("graph.png"), chart, 1000, 500);
    }

    public PieDataset createDataset(Map<String, Long> data) {
        DefaultPieDataset dataset = new DefaultPieDataset();
        data.forEach(dataset::setValue);
        return dataset;
    }
}

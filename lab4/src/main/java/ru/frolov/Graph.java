package ru.frolov;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Graph {
    public void draw(List<Double> x, List<Double> xFilt) throws IOException {
        JFreeChart chart = ChartFactory.createLineChart(
                "SMA graph",
                "value",
                "period",
                createDataset(xFilt, x),
                PlotOrientation.VERTICAL,
                true,
                true,
                false
        );

        ChartUtils.saveChartAsPNG(new File("sma.png"), chart, 500, 500);
    }

    private DefaultCategoryDataset createDataset(List<Double> x, List<Double> xFilt) {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        for (int i = 0; i < x.size(); i++) {
            dataset.addValue(x.get(i), "data", "" + i);
        }

        for (int i = 0; i < xFilt.size(); i++) {
            dataset.addValue(xFilt.get(i), "simple average", "" + i);
        }
        return dataset;
    }
}

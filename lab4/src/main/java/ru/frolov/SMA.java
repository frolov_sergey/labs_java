package ru.frolov;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class SMA {
    private final Queue<Double> dataset = new LinkedList<>();
    private final int period;
    private double sum;

    public SMA(int period) {
        this.period = period;
    }

    public List<Double> calculate(List<Double> source) {
        List<Double> result = new ArrayList<>();
        source.forEach(i -> {
            addData(i);
            result.add(getMean());
        });
        return result;
    }

    public void addData(double num) {
        sum += num;
        dataset.add(num);

        if (dataset.size() > period) {
            sum -= dataset.remove();
        }
    }

    public double getMean() {
        return sum / period;
    }
}


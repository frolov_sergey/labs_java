package ru.frolov;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {
    public static void generate_data(List<Double> arr, int length){
        Random rand = new Random();
        for (int i = 0; i < length; i++)
            arr.add(rand.nextDouble());
    }

    public static void main(String[] args) throws IOException {
        int length = 55;
        List<Double> x = new ArrayList<Double>();
        generate_data(x, length);

        SMA sma = new SMA(5);
        List<Double> x_filt = sma.calculate(x);
        Graph graph = new Graph();
        graph.draw(x, x_filt);
    }
}
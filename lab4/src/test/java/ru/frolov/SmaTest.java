package ru.frolov;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class SmaTest {
    @Test
    @DisplayName("Проверка рассчета sma")
    public void test1() {
        List<Double> source = List.of(1.0, 2.0, 3.0);
        SMA sma = new SMA(5);
        List<Double> result = sma.calculate(source);
        Assertions.assertEquals(source.size(), result.size());
        for (int i = 0; i < source.size(); i++) {
            Assertions.assertNotEquals(source.get(i), result.get(i));
        }
    }

    @Test
    @DisplayName("Проверка генерации данных")
    public void test2() {
        List<Double> result = new ArrayList<>();
        Main.generate_data(result, 10);
        Assertions.assertEquals(10, result.size());

        result.clear();
        Main.generate_data(result, 50);
        Assertions.assertEquals(50, result.size());
    }
}

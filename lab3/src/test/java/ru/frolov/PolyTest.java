package ru.frolov;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.frolov.poly.Calculator;
import ru.frolov.poly.Parser;

import java.util.List;

public class PolyTest {
    @Test
    @DisplayName("Проверка parser")
    public void test1() {
        Parser parser = new Parser();
        Assertions.assertIterableEquals(List.of(), parser.parse(""));
        Assertions.assertIterableEquals(List.of(1.0, 2.0, 3.0), parser.parse("1,2,3"));
        Assertions.assertIterableEquals(List.of(10.0, 30.0, 3.3), parser.parse("10, 30, 3.3"));
    }

    @Test
    @DisplayName("Проверка calculator")
    public void test2() {
        Calculator calculator = new Calculator();
        Assertions.assertEquals(0.6166666666666667, calculator.calculate(List.of(1.0, 2.0, 3.0)));
        Assertions.assertEquals(0.15070436809567245, calculator.calculate(List.of(10.0, 20.0, 30.0)));
        Assertions.assertEquals(0.27424242424242423, calculator.calculate(List.of(7.0, 8.0, 9.0)));
        Assertions.assertEquals(0.5, calculator.calculate(List.of(3.0, 3.0, 3.0)));
    }
}

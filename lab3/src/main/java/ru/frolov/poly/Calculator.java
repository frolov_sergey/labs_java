package ru.frolov.poly;

import java.util.Iterator;
import java.util.List;

public class Calculator {
    public static void main(String[] args) {}

    public double calculate(List<Double> values) {
        double result = 0.0;

        Iterator<Double> foreach = values.iterator();
        while (foreach.hasNext()) result += 1.0 / (3.0 + foreach.next());

        return result;
    }
}

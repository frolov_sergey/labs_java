package ru.frolov;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import ru.frolov.poly.Calculator;
import ru.frolov.poly.Parser;

import java.util.List;

public class Main {
    Options options = new Options();

    Option poly = new Option("p", "poly", true, "input polynomial values");

    public static void main(String[] args) {
        Main m = new Main();
        m.init_options();

        Parser parser = new Parser();
        Calculator calc = new Calculator();

        List<Double> values = parser.parse(m.options, args);
        System.out.println("Result: " + calc.calculate(values));
    }

    private void init_options(){
        poly.setRequired(true);
        options.addOption(poly);
    }
}

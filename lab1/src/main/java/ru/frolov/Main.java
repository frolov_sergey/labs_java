package ru.frolov;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner launch = new Scanner(System.in);
        System.out.println("Welcome to ru.frolov.Calculator program!\nEnter GO if you want to start a calculation");
        String launch_cmd = launch.nextLine();

        if (launch_cmd.equals("GO")) {
            Calculator c = new Calculator();
            c.calculate();
        }
    }
}
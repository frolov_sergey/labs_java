package ru.frolov;

import java.util.Scanner;

public class Calculator {
    private final Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Launch ru.frolov.Calculator...");
    }

    public void calculate(){
        while(true) {
            System.out.println("Enter the first value");
            String first_val = input.nextLine();
            System.out.println("Enter the operation (+, -, *, /)");
            String operation = input.nextLine();
            System.out.println("Enter the second value");
            String second_val = input.nextLine();

            double value1 = 0.0;
            double value2 = 0.0;

            try {
                value1 = Double.valueOf(first_val);
            }
            catch (NumberFormatException e) {
                System.out.println("Invalid first value convertion. Please try again or type exit to exit program");
                String exit = input.nextLine();
                if(exit.equals("exit"))
                    break;
                else
                    continue;
            }
            try {
                value2 = Double.valueOf(second_val);
            }
            catch (NumberFormatException e) {
                System.out.println("Invalid second value convertion. Please try again or type exit to exit program");
                String exit = input.nextLine();
                if(exit.equals("exit"))
                    break;
                else
                    continue;
            }

            Double result = calculate(operation, value1, value2);
            if (result == null) {
                continue;
            } else {
                System.out.println("Result: " + calculate(operation, value1, value2));
            }

            System.out.println("If you want continue the calculations, enter y");
            String continue_cmd = input.nextLine();
            if(!continue_cmd.equals("y"))
                break;
        }
    }

    public Double calculate(String operation, double value1, double value2) {
        if ("+".equals(operation)) {
            return value1 + value2;
        } else if ("-".equals(operation)) {
            return value1 - value2;
        } else if ("*".equals(operation)) {
            return value1 * value2;
        } else if ("/".equals(operation)) {
            try {
                if ((1 / value2) == Double.POSITIVE_INFINITY)
                    throw new ArithmeticException("Error");
                else
                    System.out.print("Result: ");
                return value1 / value2;
            } catch (ArithmeticException e) {
                System.out.println("Calculation error, division by zero");
            }
        } else {
            System.out.println("Incorrect operator.Please, try again or type exit to exit program");
            String exit = input.nextLine();
            if (exit.equals("exit")) {
                return null;
            }
        }

        return null;
    }
}
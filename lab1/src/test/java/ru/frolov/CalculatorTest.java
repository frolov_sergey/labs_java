package ru.frolov;

import org.junit.jupiter.api.*;

public class CalculatorTest {
    @Test
    @DisplayName("Проверка сложения")
    public void test1() {
        Calculator calculator = new Calculator();
        Assertions.assertEquals(2, calculator.calculate("+", 1.0, 1.0));
        Assertions.assertEquals(5, calculator.calculate("+", 2.0, 3.0));
        Assertions.assertEquals(20, calculator.calculate("+", 10.0, 10.0));
        Assertions.assertEquals(100, calculator.calculate("+", 50.0, 50.0));
        Assertions.assertEquals(250, calculator.calculate("+", 125.0, 125.0));
    }

    @Test
    @DisplayName("Проверка вычитания")
    public void test2() {
        Calculator calculator = new Calculator();
        Assertions.assertEquals(0, calculator.calculate("-", 1.0, 1.0));
        Assertions.assertEquals(-1, calculator.calculate("-", 2.0, 3.0));
        Assertions.assertEquals(40, calculator.calculate("-", 50.0, 10.0));
        Assertions.assertEquals(450, calculator.calculate("-", 500.0, 50.0));
        Assertions.assertEquals(100, calculator.calculate("-", 225.0, 125.0));
    }

    @Test
    @DisplayName("Проверка умножения")
    public void test3() {
        Calculator calculator = new Calculator();
        Assertions.assertEquals(1, calculator.calculate("*", 1.0, 1.0));
        Assertions.assertEquals(6, calculator.calculate("*", 2.0, 3.0));
        Assertions.assertEquals(500, calculator.calculate("*", 50.0, 10.0));
        Assertions.assertEquals(25000, calculator.calculate("*", 500.0, 50.0));
    }

    @Test
    @DisplayName("Проверка деления")
    public void test4() {
        Calculator calculator = new Calculator();
        Assertions.assertEquals(1, calculator.calculate("/", 1.0, 1.0));
        Assertions.assertEquals(1.5, calculator.calculate("/", 3.0, 2.0));
        Assertions.assertEquals(5, calculator.calculate("/", 50.0, 10.0));
        Assertions.assertEquals(10, calculator.calculate("/", 500.0, 50.0));
    }
}

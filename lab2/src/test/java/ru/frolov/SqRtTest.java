package ru.frolov;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class SqRtTest {
    @Test
    @DisplayName("Проверка квадратнго корня")
    public void test1() {
        SqRt sqRt = new SqRt();
        Assertions.assertEquals(3, sqRt.calculate(9));
        Assertions.assertEquals(9, sqRt.calculate(81));
        Assertions.assertEquals(12, sqRt.calculate(144));
        Assertions.assertEquals(90, sqRt.calculate(8100));
    }
}
